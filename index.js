const express = require('express');
const ejs = require('ejs');
const fs = require('fs');
const moment = require('moment');

var app = express();
var weatherFile = `${__dirname}/log/weather.log`;

app.use(express.static('public'));
app.use(express.static('img'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.render('index');
});

app.get('/weather-data', (req, res) => {
  res.json(readWeather());
});

function readWeather() {
  var weather = fs.readFileSync(weatherFile, {encoding: 'utf8'}); // てきとー
  return weather.split('\n')
    .map((l) => l.split(','))
    .filter((l) => l[0].length > 0)
    .map((l) => {
      return {
        date: moment(l[0]).toDate(),
        level: parseInt(l[1], 10),
        path: l[2]
      };
    });
}

app.listen(process.env.PORT || 3000);
