var margin = {top: 30, right: 40, bottom: 30, left: 40};
var height = 200 - margin.top - margin.bottom;
var width = 500 - margin.left - margin.right;
var minLevel = -1;
var maxLevel = 13;

var bisectDate = d3.bisector(function(d) { return d.date; }).left;

var x = d3.time.scale()
      .range([0, width]);
var y = d3.scale.linear()
      .range([0, height]);
var xAxis = d3.svg.axis()
      .scale(x)
      .orient('bottom');
var yAxis = d3.svg.axis()
      .scale(y)
      .orient('left');
var line = d3.svg.line()
      .x((d) => x(d.date))
      .y((d) => y(d.level));

var img = d3.select('#map img');

var svg = d3.select('#map')
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
    .append('g')
      .attr("transform", `translate(${margin.left}, ${margin.top})`);

d3.json('/weather-data', (data) => {
  data.forEach((d) => {
    d.date = new Date(d.date);
    d.level = +d.level;
  });

  img.attr('src', data[data.length - 1].path);

  x.domain([data[0].date, data[data.length - 1].date]);
  y.domain([maxLevel, minLevel]);

  svg.append('g')
    .attr('class', 'x axis')
    .attr('transform', `translate(0, ${height})`)
    .call(xAxis);

  svg.append('g')
    .attr('class', 'y axis')
    .call(yAxis)
  .append('text')
    .attr('transform', 'rotate(-90)')
    .attr('y', 6)
    .attr('dy', '.71em')
    .style('text-anchor', 'end')
    .text('Lv');

  svg.selectAll('circle')
    .data(data)
    .enter()
  .append('circle')
    .attr('class', 'circle')
    .attr('r', 1.5)
    .attr('cx', (d) => x(d.date))
    .attr('cy', (d) => y(d.level));

  // svg.append('path')
  //   .datum(data)
  //   .attr('class', 'line')
  //   .attr('d', line);

  svg.append("rect")
    .attr("class", "overlay")
    .attr("width", width)
    .attr("height", height)
    .on("mousemove", mousemove);

  function mousemove() {
    var x0 = x.invert(d3.mouse(this)[0]);
    var i = bisectDate(data, x0, 1);
    var d0 = data[i - 1];
    var d1 = data[i];
    var d = x0 - d0.date > d1.date - x0 ? d1 : d0;
    img.attr('src', d.path);
  }
});
