"use strict";

const Promise = require('bluebird');
const weather = require('farm-matsumoto-wather');
const moment = require('moment');
const join = require('path').join;
const basename = require('path').basename;
const rename = Promise.promisify(require('fs').rename);

const imgPath = join(__dirname, '..', 'public', 'img');

function processPath(path) {
  return join('/img', basename(path).replace(/\?/, '_'));
}

let now = moment();
weather(now, imgPath)
  .then((res) => {
    console.log(`${now.format()},${res.level},${processPath(res.path)}`);
    return res.path;
  })
  .then((path) => rename(path, path.replace(/\?/, '_')))
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
